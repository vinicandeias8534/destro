package robots;

import robocode.*;
import robocode.util.Utils;

import java.awt.*;

public class Destro extends AdvancedRobot {
    public void run() {
        setBodyColor(new Color(102, 51, 0));

        setGunColor(Color.DARK_GRAY);

        setRadarColor(Color.lightGray);

        setScanColor(Color.white);

        setBulletColor(Color.black);
        while (true) {

            this.ahead(100.0);
            this.turnGunRight(360.0);
            this.back(100.0);
            this.turnGunRight(360.0);

            execute();
        }
    }
    public void onScannedRobot(ScannedRobotEvent event) {

        double anguloAbsoluto = getHeading() + event.getBearing();
        double anguloCanhao = Utils.normalRelativeAngleDegrees(anguloAbsoluto - getGunHeading());

        if (anguloCanhao > 0) {
            turnGunRight(anguloCanhao);
        } else {
            turnGunLeft(-anguloCanhao);
        }

        if (Math.abs(anguloCanhao) <= 3) {
            if (getEnergy() > 30) {
                fireBullet(3);
            } else {
                fire(2);
            }
        }
        scan();
    }

    public void onHitWall(HitWallEvent event) {
        reverseDirection();
    }

    public void onHitRobot(HitRobotEvent event) {
        if (event.getBearing() > -10.0 && event.getBearing() < 10.0) {
            this.fire(3.0);
        }else{
            reverseDirection();
        }

    }

    public void onHitByBullet(HitByBulletEvent e) {

        this.turnLeft(90.0 - e.getBearing());
    }
    private void reverseDirection() {
        ahead(-100);
        turnRight(90);
    }
}
